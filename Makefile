CC=gcc
CFLAGS=-I. -std=c99 -Wall -Werror -D_POSIX_C_SOURCE=199309L
DEPS = header.h
OBJ = implementation.o
LDLIBS = -lpthread

.PHONY: all clean
all: main
	
main: main.o $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -f *.o main
	
