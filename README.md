# Operating Systems Lab 2017 - Exam 1 - 2nd Try #

In this exercise, a multi-process system should be implemented. Which fulfills this overall structure:

![Illustration](images/illustration.png)

A main process starts 3 `dispatcher` processes, each of which is connected to the main process using a pipe.

Each `dispatcher` process launches one `writer`, which is connected to the dispatcher using a shared memory segment.

When the main process sends a `log_msg` (with `dispatcher_send_msg`), this message is first transmitted over a FIFO to the dispatcher process. This process then writes it to its shared memory segment. After this, it signals its associated `writer` using `SIGUSR1`. After receiving the signal, the writer uses `print_message` to print the message, and sends `SIGUSR2` back to its associated dispatcher to indicate that it is ready to receive another message.

### Structure of this repository ###

These files are already available in the repository:

* a `Makefile` for building the program
* `header.h` containing the necessary structure definitions and function declarations
* `implementation.c` which contains the implementation of the functions defined in the header
* `main.c` which implements a test program that is already pre-defined

### Your task ###

Implement the behavior of the system as specified above. This includes starting and stopping processes, and allocating and using the required FIFOs, shared memory segments and signal handlers.

Take note that:

* You are **not** allowed to change the interface of any of the provided functions or the main program
* You **are** allowed to add additional data members to all structures as required
* You **are** allowed to add additional functions as required
