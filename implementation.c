#include "header.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

/// Dispatcher

void dispatcher_init(dispatcher* d) {
	// TODO
	// 1) Create FIFO to communicate from main process to dispatcher process
	// 2) launch process for dispatcher
}

void dispatcher_destroy(dispatcher* d) {
	// TODO
	// End the process created for the dispatcher and clean up the resources allocated for it
}

void dispatcher_send_msg(dispatcher* d, log_msg* msg) {
	// TODO
	// Send message "msg" on FIFO to dispatcher "d"
}

// TODO any other functions required for dispatcher

/// Writer

void writer_init(writer* w) {
	// TODO - should be called from dispatcher process
	// 1) Create shared memory segment to communicate from main dispatcher to writer process
	// 2) launch process for writer
}

// TODO any other functions required for writer

/// Messaging -- no need to change any of this

void set_msg_string(log_msg* msg) {
	int num_msg_strings = 5;
	switch(rand() % num_msg_strings) {
	case 0: strcpy(msg->msg, "Hellooo."); break;
	case 1: strcpy(msg->msg, "Searching."); break;
	case 2: strcpy(msg->msg, "Sentry mode activated.."); break;
	case 3: strcpy(msg->msg, "Is anyone there?"); break;
	case 4: strcpy(msg->msg, "Could you come over here?"); break;
	}
}

void print_message(log_msg* msg) {
	printf("writer %8d: msg %3d - %s\n", getpid(), msg->id, msg->msg);
}
