
#include "header.h"

#include <stdlib.h>

#define NUM_DISPATCHERS 3

#define NUM_MSG 32

int main(int argc, char** argv) {

	dispatcher dispatchers[3];

	for(int i = 0; i < NUM_DISPATCHERS; ++i) {
		dispatcher_init(&dispatchers[i]);
	}

	for(int i = 0; i < NUM_MSG; ++i) {
		log_msg msg;
		msg.id = i;
		set_msg_string(&msg);
		dispatcher_send_msg(&dispatchers[rand()%NUM_DISPATCHERS], &msg);
	}

	for(int i = 0; i < NUM_DISPATCHERS; ++i) {
		dispatcher_destroy(&dispatchers[i]);
	}
}
