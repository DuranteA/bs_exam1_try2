#pragma once

/// Messaging -- no need to change any of this

typedef struct log_msg {
	int id;
	char msg[64];
} log_msg;

void set_msg_string(log_msg* msg);

void print_message(log_msg* msg);

/// Dispatcher

typedef struct dispatcher {
	// TODO
} dispatcher;

void dispatcher_init(dispatcher* d);

void dispatcher_destroy(dispatcher* d);

void dispatcher_send_msg(dispatcher* d, log_msg* msg);

// TODO any other functions required for dispatcher

/// Writer

typedef struct writer {
	// TODO
} writer;

void writer_init(writer* w);

// TODO any other functions required for writer
